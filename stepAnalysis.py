import pandas as pd
import numpy as np
import json

def getStepIndex(data):
    data -= np.average(data)
    step = np.hstack((np.ones(len(data)), -1*np.ones(len(data))))
    data_step = np.convolve(data, step, mode='valid')
    return np.argmax(data_step) + 1

def getDataAtStep(file, trigger):
    return file.loc[getStepIndex(file[trigger]), :]

def processAllFiles():
    with open("stepAnalysisFiles.json") as filesJson:
        infoList = json.load(filesJson)
        for fileInfo in infoList["files"]:
            print("Step Levels for", fileInfo["name"], ": ")
            data = pd.read_csv(fileInfo["fileName"])
            print(getDataAtStep(data, fileInfo["trigger"]))
            print()

processAllFiles()
input("Press Any Key to Continue...")
